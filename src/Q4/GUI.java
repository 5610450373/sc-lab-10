package Q4;
import java.awt.BorderLayout;
import java.awt.Button;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ButtonGroup;
import javax.swing.GroupLayout.Group;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextArea;
import javax.swing.plaf.basic.BasicBorders.RadioButtonBorder;


public class GUI extends JFrame  { 
	JComboBox box;
	JPanel pan2;
	JPanel pan1;
	public GUI(){
		createFrame();
	}
	public void createFrame(){
		pan1 = new JPanel();
		pan2 = new JPanel();
		pan2.setBackground(new Color(255,255,255));
		box = new JComboBox();
		box.addItem("");
		box.addItem("RED");
		box.addItem("GREEN");
		box.addItem("BLUE");
		pan1.add(box);
		setLayout(new BorderLayout());
		add(pan2);
		add(pan1,BorderLayout.SOUTH);
		box.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				if(box.getSelectedItem().equals("RED")){
					pan2.setBackground(new Color(255,0,0));
				}
				else if(box.getSelectedItem().equals("BLUE")){
					pan2.setBackground(new Color(0,0,255));
				}
				else if(box.getSelectedItem().equals("GREEN")){
					pan2.setBackground(new Color(0,255,0));
				}
				else{
					pan2.setBackground(new Color(255,255,255));
				}
			}
		});
		
		
	}	
}
