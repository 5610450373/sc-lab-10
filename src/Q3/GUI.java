package Q3;
import java.awt.BorderLayout;
import java.awt.Button;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ButtonGroup;
import javax.swing.GroupLayout.Group;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextArea;
import javax.swing.plaf.basic.BasicBorders.RadioButtonBorder;


public class GUI extends JFrame  {
	JCheckBox Rbut;
	JCheckBox Gbut;
	JCheckBox Bbut;
	JPanel pan2;
	JPanel pan1;
	public GUI(){
		createFrame();
	}
	public void createFrame(){
		pan1 = new JPanel();
		pan2 = new JPanel();
		pan2.setBackground(new Color(255,255,255));
		Rbut = new JCheckBox("RED");
		Gbut = new JCheckBox("GREEN");
		Bbut = new JCheckBox("BLUE");
		
		
		pan1.add(Rbut);
		pan1.add(Gbut);
		pan1.add(Bbut);
		setLayout(new BorderLayout());
		add(pan2);
		add(pan1,BorderLayout.SOUTH);
		
		class Listener implements ActionListener{
			@Override
			public void actionPerformed(ActionEvent arg0) {
				if(Rbut.isSelected()){
				pan2.setBackground(new Color(255, 0, 0));
				}
				else if(Gbut.isSelected()){
					pan2.setBackground(new Color(0,255, 0));
				}
				else if(Bbut.isSelected()){
					pan2.setBackground(new Color(0, 0,255));
				}
				if(Rbut.isSelected()&&Gbut.isSelected()){
					pan2.setBackground(new Color(255,255, 0));
				}
				if(Rbut.isSelected()&&Bbut.isSelected()){
					pan2.setBackground(new Color(255, 0, 255));
				}
				if(Gbut.isSelected()&&Bbut.isSelected()){
					pan2.setBackground(new Color(0, 255, 255));
				}
				if(Rbut.isSelected()&&Gbut.isSelected()&&Bbut.isSelected()){
					pan2.setBackground(new Color(0, 255, 255));
				}
				if(!Rbut.isSelected()&&!Gbut.isSelected()&&!Bbut.isSelected()){
					pan2.setBackground(new Color(255,255,255));
				}
			}
		}
		Rbut.addActionListener(new Listener());
		Gbut.addActionListener(new Listener());
		Bbut.addActionListener(new Listener());
	}	
}
