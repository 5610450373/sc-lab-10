package Q5;
import java.awt.BorderLayout;
import java.awt.Button;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ButtonGroup;
import javax.swing.GroupLayout.Group;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextArea;
import javax.swing.plaf.basic.BasicBorders.RadioButtonBorder;


public class GUI extends JFrame  { 
	JMenu Menu;
	JMenuBar Mbar;
	JMenuItem R,G,B;
	JPanel pan2;
	JPanel pan1;
	public GUI(){
		createFrame();
	}
	public void createFrame(){
		pan1 = new JPanel();
		pan2 = new JPanel();
		pan2.setBackground(new Color(255,255,255));
		Menu = new JMenu("MENU");
		R = new JMenuItem("RED");
		G = new JMenuItem("GREEN");
		B = new JMenuItem("BLUE");
		Menu.add(R);
		Menu.add(G);
		Menu.add(B);
		pan1.add(Menu);
		setLayout(new BorderLayout());
		add(pan2);
		add(pan1,BorderLayout.SOUTH);
		Mbar = new JMenuBar();
		setJMenuBar(Mbar);
		Mbar.add(Menu);
		
		R.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				pan2.setBackground(new Color(255,0,0));
			}
		});
		
		G.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				pan2.setBackground(new Color(0,255,0));
			}
		});
		
		B.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				pan2.setBackground(new Color(0,0,255));
			}
		});
		
		
		
		
	}	
}
