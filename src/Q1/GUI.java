package Q1;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextArea;


public class GUI extends JFrame  {
	JButton Rbut;
	JButton Gbut;
	JButton Bbut;
	JPanel pan2;
	JPanel pan1;
	public GUI(){
		createFrame();
	}
	public void createFrame(){
		pan1 = new JPanel();
		pan2 = new JPanel();
		pan2.setBackground(new Color(255,255,255));
		Rbut = new JButton("RED");
		Gbut = new JButton("GREEN");
		Bbut = new JButton("BLUE");
		pan1.add(Rbut);
		pan1.add(Gbut);
		pan1.add(Bbut);
		setLayout(new BorderLayout());
		add(pan2);
		add(pan1,BorderLayout.SOUTH);
		
		class RedListener implements ActionListener{
			@Override
			public void actionPerformed(ActionEvent arg0) {
				pan2.setBackground(new Color(255, 0, 0));
				
			}
		}
		class GreenListener implements ActionListener{
			public void actionPerformed(ActionEvent e){
				pan2.setBackground(new Color(0,255,0));
			}
		}
		class BlueListener implements ActionListener{
			public void actionPerformed(ActionEvent e){
				pan2.setBackground(new Color(0,0,255));
			}
		}
		
		Rbut.addActionListener(new RedListener());
		Gbut.addActionListener(new GreenListener());
		Bbut.addActionListener(new BlueListener());
	}	
}
