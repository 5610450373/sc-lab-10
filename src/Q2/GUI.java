package Q2;
import java.awt.BorderLayout;
import java.awt.Button;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ButtonGroup;
import javax.swing.GroupLayout.Group;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextArea;
import javax.swing.plaf.basic.BasicBorders.RadioButtonBorder;


public class GUI extends JFrame  {
	JRadioButton Rbut;
	JRadioButton Gbut;
	JRadioButton Bbut;
	JPanel pan2;
	JPanel pan1;
	public GUI(){
		createFrame();
	}
	public void createFrame(){
		pan1 = new JPanel();
		pan2 = new JPanel();
		pan2.setBackground(new Color(255,255,255));
		Rbut = new JRadioButton("RED");
		Gbut = new JRadioButton("GREEN");
		Bbut =new JRadioButton("BLUE");
		ButtonGroup G = new ButtonGroup();
		G.add(Rbut);
		G.add(Gbut);
		G.add(Bbut);
		pan1.add(Rbut);
		pan1.add(Gbut);
		pan1.add(Bbut);
		setLayout(new BorderLayout());
		add(pan2);
		add(pan1,BorderLayout.SOUTH);
		
		class RedListener implements ActionListener{
			@Override
			public void actionPerformed(ActionEvent arg0) {
				pan2.setBackground(new Color(255, 0, 0));
				
			}
		}
		class GreenListener implements ActionListener{
			public void actionPerformed(ActionEvent e){
				pan2.setBackground(new Color(0,255,0));
			}
		}
		class BlueListener implements ActionListener{
			public void actionPerformed(ActionEvent e){
				pan2.setBackground(new Color(0,0,255));
			}
		}
		
		Rbut.addActionListener(new RedListener());
		Gbut.addActionListener(new GreenListener());
		Bbut.addActionListener(new BlueListener());
	}	
}
