package Q7;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.border.Border;
import javax.swing.text.JTextComponent;


public class GUI extends JFrame  {
	BankAccount acc;
	JButton wdbut;
	JButton dpbut;
	JTextArea text1,text3;
	JLabel text2;
	JPanel pan1,pan2,pan3;
	public GUI(){
		acc = new BankAccount();
		createFrame();
	}
	public void createFrame(){
		pan1 = new JPanel();
		pan2 = new JPanel();
		pan3 = new JPanel();
		text1 = new JTextArea();
		text2 = new JLabel("Balance = "+"0");
		text3 = new JTextArea();
		wdbut = new JButton("WD");
		dpbut = new JButton("DP");
		pan1.add(wdbut);
		pan1.add(dpbut);
		pan2.setLayout(new GridLayout(2,2));
		pan2.add(text1);
		pan2.add(text3);
		pan2.add(text2);
		pan3.setLayout(new GridLayout(1,2));
		pan3.add(new JLabel("	BankAccount"));
		setLayout(new BorderLayout());
		add(pan3,BorderLayout.NORTH);
		add(pan2);
		add(pan1,BorderLayout.SOUTH);
		
		wdbut.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				String s = text1.getText();
				acc.wd(Double.parseDouble(s));
				text2.setText("Balance = "+acc.getbalance());
				String ss =text3.getText();
				ss =ss+"\n"+"WD : "+s;
				text3.setText(ss);
			}
		});
		dpbut.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				String s = text1.getText();
				acc.dp(Double.parseDouble(s));
				text2.setText("Balance = "+acc.getbalance());
				String ss =text3.getText();
				ss =ss+"\n"+"DP : "+s;
				text3.setText(ss);
			}
		});
	}	
}
